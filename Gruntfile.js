module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-sass');

	grunt.initConfig({

        // This task just take the js and minify it
		uglify: {
			options: {
				mangle: false
			},
			my_target: {
				footer: ';',
				banner: ';',
				files: {
    				'./data/public/site.min.js': ['./data/js/vendor/*.js', './data/public/site.js']
				}
			}
		},

        // This task just combine all the css file and minimize them
		cssmin: {
		  combine: {
		    files: {
				'./data/public/site.min.css': [
				'./data/css/vendor/*.min.css',
				'./data/public/main.css'
		      ]
		    }
		  }
		},

        // Compile sass file
		sass: {
			options: {
				sourceMap: true
			},
			dist: {
				files: {
					'./data/public/main.css': './data/css/sass/main.scss'
				}
			}
		},

        // Whatch for changes
		watch: {
			sass: {
				files: './data/css/sass/*.scss',
				tasks: ['sass', 'cssmin']
			},

            js: {
                files: './data/js/**/*.js',
                tasks: ['browserify', 'uglify']
            }
		},

        // Browserify our js
        browserify: {
            dist: {
                files: {
                    './data/public/site.js': ['./data/js/main.js']
                }
            }
        }
	});

    // Compile everything into the correct packages
    grunt.registerTask('default', ['browserify', 'uglify', 'sass', 'cssmin']);

    // Watch for the changes and recompile if needed
	grunt.registerTask('local', ['watch']);
}

