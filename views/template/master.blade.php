<!doctype html>
<html>
<head>
    <meta charset="{{ bloginfo( 'charset' ) }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <!-- favicon -->
    <link rel="icon" href="{{ get_site_url() }}/favicon.ico" type="image/x-icon" />
    <link rel="icon" type="image/ico" href="{{ get_template_directory_uri() }}/images/favicon.ico"/>

    <title> {{ wp_title( '|', true, 'right' ) }} </title>

    {{ wp_head() }}

    <!--[if lt IE 9]>
        <script src="{{ get_template_directory_uri() }}/js/vendor/html5.js" type="text/javascript"></script>
        <script src="{{ get_template_directory_uri() }}/js/vendor/respond.js" type="text/javascript"></script>
    <![endif]-->
</head>
<body {{ body_class() }}>
<?php include_once(get_template_directory() . '/images/symbols.svg'); ?>
    <div id="sb-site">
        <!-- Include header -->
        @include('views.template.header')

        <div class="layoutMain">
            <!-- Implement the proper layout -->
            @yield('main')
        </div>

        <!-- Include footer -->
        @include('views.template.footer')

    </div>

    <!-- Include offcanvas -->
    @include('views.template.offcanvas')
    {{ wp_footer() }}

    <script>
        (function($) {
            $(document).ready(function() {
                $.slidebars();
            });
        }) (jQuery);
    </script>
</body>
</html>
