<div class="sb-slidebar sb-left sb-style-overlay">
    <?php
        wp_nav_menu(array(
                        'menu' => 'mainmenu',
                        'container' => 'nav',
                        'theme_location' => 'primary',
                        'menu_class' => 'nav-menu',
                        'container_id' => 'mainmenunav',
                        'container_class' => 'hidden-md')
        );
    ?>
</div>
