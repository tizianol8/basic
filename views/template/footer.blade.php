<footer class="blockFooter">
    <div class="center">
        <div class="blockFooter__item blockFooter__item_text">
            <?php if ( is_active_sidebar('footer-text')) : ?>
                <?php dynamic_sidebar('footer-text'); ?>
            <?php endif; ?>
        </div>
        <div class="blockFooter__item blockFooter__item_akei">
            <a href="https://www.akei.it" target="_blank">
                concept by <img style="position: absolute; margin-left: 5px;" alt="Siti internet, e-commerce, grafica pubblicitaria in Trentino" src="https://www.akei.it/images/akei_logo_concept_rose.png">
            </a>
        </div>
    </div>
</footer>
