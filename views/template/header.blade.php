<header>
    <div class="blockHead">
        <div class="center">
        </div>
    </div>
    <div class="center">
        <div class="blockMenuLine">
            <div class="blockMenuLine__item blockMenuLine__item_logo">
                <a href="/" title="Bonvecchio marmi" class="blockMenuLine__logoLink">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo" alt="Logo sitename" class="blockMenuLine__logo">
                </a>
            </div>
            <div class="blockMenuLine__item blockMenuLine__item_menu">
                <div class="blockMenu">
                    <?php
                    wp_nav_menu( array(
                            'container' => 'nav',
                            'theme_location' => 'primary',
                            'menu_class' => 'blockMenu__list',
                            'container_class' => 'blockMenu__wrapper',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul><span class="stretcher"></span>'
                    ));
                    ?>
                </div>
                <a href="#" class="sb-toggle-left blockMenuLine__burger blockBurger"> Menu mobile </a>
            </div>
        </div>
    </div>
</header>
