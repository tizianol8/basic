@extends('views.template.master')
@section('main')
    <div class="center layoutRedazionale">
        <h1>{{ get_the_title() }}</h1>
        <?php the_content(); ?>
    </div>
@endsection
