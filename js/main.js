var cookie = require('./modules/cookie.js');

$(document).ready(function()
{
	// Activate slimbox when click on an image
	$('.gallery a').swipebox();

	// Scroll to anchor
	// Used to scroll to an anchor element
	$("a").click(function()
	{
		if($(this).attr("href")[0] == '#' && $(this).attr("href").length > 1) {
			var anchor = $(this).attr("href").substr(1);
			var target = $("a[name='" + anchor + "']");
			$('html,body').animate({scrollTop: target.offset().top}, 'slow');
		}
	});
});


