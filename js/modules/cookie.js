$(document).ready(function(e) {
	var readed = Cookies.get("policy_readed");
	if(readed) {
		// Hide the cookie notice
		$('.blockCookieNotice').hide();
	} 

	$("#cookieButton").click(function(e) {
		Cookies.set("policy_readed", "1", {expires: 1});
		$('.blockCookieNotice').hide();
	});
});
