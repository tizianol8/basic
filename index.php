<?php
        setup_postdata($post);

        if(is_front_page()) {
                bladerunner('views.index', ['zPost' => $zPost]);
        } else {
                bladerunner('views.redazionale', ['zPost' => $zPost]);
        }
?>
